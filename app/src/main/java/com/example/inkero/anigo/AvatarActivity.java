package com.example.inkero.anigo;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class AvatarActivity extends AppCompatActivity {

    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avatar);

        ImageView img = (ImageView) findViewById(R.id.imageView2);
        img.setClickable(true);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences mPrefs = getSharedPreferences("label", 0);


                SharedPreferences.Editor mEditor = mPrefs.edit();
                mEditor.putString("avatarId", "2").commit();

                String mString = mPrefs.getString("avatarId", "0");
                Log.d("AVATAR ID", mString);
            }
        });
    }





}
