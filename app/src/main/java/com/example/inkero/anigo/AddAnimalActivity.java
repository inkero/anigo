package com.example.inkero.anigo;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;


public class AddAnimalActivity extends AppCompatActivity {

    private FusedLocationProviderClient locClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_animal);

        requestPermission();
        locClient = LocationServices.getFusedLocationProviderClient(this);

        Button btnAdd = (Button) findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // 60.451621, 22.276932
                //writeLocation();

                if (ActivityCompat.checkSelfPermission(AddAnimalActivity.this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                locClient.getLastLocation().addOnSuccessListener(AddAnimalActivity.this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if(location != null){
                            writeLocation(location);
                        }
                    }
                });

                Intent intent = new Intent(AddAnimalActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, 1);
    }

    private void writeLocation(Location location) {

        try {
            FileOutputStream fos = null;
            fos = openFileOutput("locations.json", MODE_PRIVATE);

            JSONObject jsonLocation = new JSONObject();
            try {
                jsonLocation.put("lat", location.getLatitude());
                jsonLocation.put("lng", location.getLongitude());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            JSONArray jsonArray = new JSONArray();
            jsonArray.put(jsonLocation);

            fos.write(jsonArray.toString().getBytes());
            Toast.makeText(AddAnimalActivity.this, "Saved to " + getFilesDir() + "/locations.json", Toast.LENGTH_LONG).show();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }


    private ArrayList<LatLng> readItems(int resource) throws JSONException {
        ArrayList<LatLng> list = new ArrayList<LatLng>();
        InputStream inputStream = getResources().openRawResource(resource);
        String json = new Scanner(inputStream).useDelimiter("\\A").next();
        JSONArray array = new JSONArray(json);
        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);
            double lat = object.getDouble("lat");
            double lng = object.getDouble("lng");
            list.add(new LatLng(lat, lng));
        }
        return list;
    }

}

